package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class IndexController {

    @RequestMapping("/index")
    @ResponseBody
    public String index(){
        return "Hello World";
    }


    @RequestMapping("/account/login")
    public ModelAndView login(String username,String password){
        ModelAndView modelAndView = new ModelAndView("/account/login");
        modelAndView.addObject("username",username);
        modelAndView.addObject("password",password);
        return modelAndView;
    }

}
